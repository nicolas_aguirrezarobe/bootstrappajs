﻿using Abp.Web.Mvc.Views;

namespace Nicolas.BootstrAppAjs.Mvc.Views
{
    public abstract class BootstrAppAjsWebViewPageBase : BootstrAppAjsWebViewPageBase<dynamic>
    {

    }

    public abstract class BootstrAppAjsWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected BootstrAppAjsWebViewPageBase()
        {
            LocalizationSourceName = BootstrAppAjsConsts.LocalizationSourceName;
        }
    }
}