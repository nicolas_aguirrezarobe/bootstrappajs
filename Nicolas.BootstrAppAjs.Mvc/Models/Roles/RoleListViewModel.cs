﻿using System.Collections.Generic;
using Nicolas.BootstrAppAjs.Roles.Dto;

namespace Nicolas.BootstrAppAjs.Mvc.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}