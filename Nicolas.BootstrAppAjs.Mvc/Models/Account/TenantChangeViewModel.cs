﻿using Abp.AutoMapper;
using Nicolas.BootstrAppAjs.Sessions.Dto;

namespace Nicolas.BootstrAppAjs.Mvc.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}