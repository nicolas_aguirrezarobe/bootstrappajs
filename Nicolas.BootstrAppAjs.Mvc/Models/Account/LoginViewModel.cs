﻿using System.ComponentModel.DataAnnotations;

namespace Nicolas.BootstrAppAjs.Mvc.Models.Account
{
    public class LoginViewModel
    {
        [Required]
        public string UsernameOrEmailAddress { get; set; }

        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}