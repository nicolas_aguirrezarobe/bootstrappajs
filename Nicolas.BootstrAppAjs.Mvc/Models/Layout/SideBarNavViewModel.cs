﻿using Abp.Application.Navigation;

namespace Nicolas.BootstrAppAjs.Mvc.Models.Layout
{
    public class SideBarNavViewModel
    {
        public UserMenu MainMenu { get; set; }

        public string ActiveMenuItemName { get; set; }
    }
}