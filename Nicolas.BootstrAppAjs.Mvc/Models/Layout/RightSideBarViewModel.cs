using Nicolas.BootstrAppAjs.Configuration.Ui;

namespace Nicolas.BootstrAppAjs.Mvc.Models.Layout
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}