﻿using System.Collections.Generic;
using Nicolas.BootstrAppAjs.Roles.Dto;
using Nicolas.BootstrAppAjs.Users.Dto;

namespace Nicolas.BootstrAppAjs.Mvc.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}