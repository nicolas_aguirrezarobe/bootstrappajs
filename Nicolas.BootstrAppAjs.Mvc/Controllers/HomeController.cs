﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace Nicolas.BootstrAppAjs.Mvc.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : BootstrAppAjsControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}