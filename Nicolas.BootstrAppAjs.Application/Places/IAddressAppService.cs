﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Nicolas.BootstrAppAjs.Places.Dto;

namespace Nicolas.BootstrAppAjs.Places
{
    public interface IAddressAppService : IAsyncCrudAppService<AddressDto, long, PagedResultRequestDto, CreateAddressDto, AddressDto>
    {
    }
}
