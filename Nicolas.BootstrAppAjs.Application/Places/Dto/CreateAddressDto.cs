﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nicolas.BootstrAppAjs.Places.Dto
{
    public class CreateAddressDto : AddressDto
    {
        [Required]
        public override string StreetName { get => base.StreetName; set => base.StreetName = value; }

        [Required]
        public override string StreetNumber { get => base.StreetName; set => base.StreetName = value; }

        [Required]
        public override string ZipCode { get => base.ZipCode; set => base.ZipCode = value; }
    }
}
