﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nicolas.BootstrAppAjs.Places.Dto
{
    [AutoMapFrom(typeof(Address)), AutoMapTo(typeof(Address))]
    public class AddressDto : EntityDto<long>
    {
        public virtual string StreetName { get; set; }
        public virtual string StreetNumber { get; set; }
        public virtual string ZipCode { get; set; }
    }
}
