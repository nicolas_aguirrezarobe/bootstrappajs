﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Nicolas.BootstrAppAjs.Places.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

namespace Nicolas.BootstrAppAjs.Places
{
    public class AddressAppService : AsyncCrudAppService<Address, AddressDto, long, PagedResultRequestDto, CreateAddressDto, AddressDto>, IAddressAppService
    {
        public AddressAppService(IRepository<Address, long> repository) : base(repository)
        {
        }

        public override Task<PagedResultDto<AddressDto>> GetAll(PagedResultRequestDto input)
        {
            return base.GetAll(input);
        }
    }
}
