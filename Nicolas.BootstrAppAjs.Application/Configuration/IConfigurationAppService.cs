﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Nicolas.BootstrAppAjs.Configuration.Dto;

namespace Nicolas.BootstrAppAjs.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}