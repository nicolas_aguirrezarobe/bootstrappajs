﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Nicolas.BootstrAppAjs.Sessions.Dto;

namespace Nicolas.BootstrAppAjs.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
