﻿using System.Threading.Tasks;
using Abp.Auditing;
using Abp.AutoMapper;
using Nicolas.BootstrAppAjs.Sessions.Dto;

namespace Nicolas.BootstrAppAjs.Sessions
{
    public class SessionAppService : BootstrAppAjsAppServiceBase, ISessionAppService
    {
        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput();

            if (AbpSession.UserId.HasValue)
            {
                output.User = (await GetCurrentUserAsync()).MapTo<UserLoginInfoDto>();
            }

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = (await GetCurrentTenantAsync()).MapTo<TenantLoginInfoDto>();
            }

            return output;
        }
    }
}