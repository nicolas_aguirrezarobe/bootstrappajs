﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Nicolas.BootstrAppAjs.MultiTenancy.Dto;

namespace Nicolas.BootstrAppAjs.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
