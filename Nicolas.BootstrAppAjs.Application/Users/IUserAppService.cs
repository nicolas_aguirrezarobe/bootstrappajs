using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Nicolas.BootstrAppAjs.Roles.Dto;
using Nicolas.BootstrAppAjs.Users.Dto;

namespace Nicolas.BootstrAppAjs.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}