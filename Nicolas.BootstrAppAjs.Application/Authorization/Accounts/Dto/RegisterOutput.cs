﻿namespace Nicolas.BootstrAppAjs.Authorization.Accounts.Dto
{
    public class RegisterOutput
    {
        public bool CanLogin { get; set; }
    }
}