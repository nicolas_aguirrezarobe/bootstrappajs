﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Nicolas.BootstrAppAjs.Authorization.Accounts.Dto;

namespace Nicolas.BootstrAppAjs.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
