﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using Nicolas.BootstrAppAjs.EntityFramework;

namespace Nicolas.BootstrAppAjs
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(BootstrAppAjsCoreModule))]
    public class BootstrAppAjsDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<BootstrAppAjsDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
