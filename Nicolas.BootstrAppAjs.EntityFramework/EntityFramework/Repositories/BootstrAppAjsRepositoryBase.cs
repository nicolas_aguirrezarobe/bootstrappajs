﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace Nicolas.BootstrAppAjs.EntityFramework.Repositories
{
    public abstract class BootstrAppAjsRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<BootstrAppAjsDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected BootstrAppAjsRepositoryBase(IDbContextProvider<BootstrAppAjsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class BootstrAppAjsRepositoryBase<TEntity> : BootstrAppAjsRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected BootstrAppAjsRepositoryBase(IDbContextProvider<BootstrAppAjsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
