﻿using System.Data.Common;
using Abp.Zero.EntityFramework;
using Nicolas.BootstrAppAjs.Authorization.Roles;
using Nicolas.BootstrAppAjs.Authorization.Users;
using Nicolas.BootstrAppAjs.MultiTenancy;
using System.Data.Entity;
using Nicolas.BootstrAppAjs.Places;

namespace Nicolas.BootstrAppAjs.EntityFramework
{
    public class BootstrAppAjsDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...
        public IDbSet<Address> Address { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public BootstrAppAjsDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in BootstrAppAjsDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of BootstrAppAjsDbContext since ABP automatically handles it.
         */
        public BootstrAppAjsDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public BootstrAppAjsDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public BootstrAppAjsDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}
