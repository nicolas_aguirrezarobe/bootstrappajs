using System.Data.Entity.Migrations;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using Nicolas.BootstrAppAjs.Migrations.SeedData;
using EntityFramework.DynamicFilters;

namespace Nicolas.BootstrAppAjs.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<BootstrAppAjs.EntityFramework.BootstrAppAjsDbContext>, IMultiTenantSeed
    {
        public AbpTenantBase Tenant { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "BootstrAppAjs";
        }

        protected override void Seed(BootstrAppAjs.EntityFramework.BootstrAppAjsDbContext context)
        {
            context.DisableAllFilters();

            if (Tenant == null)
            {
                //Host seed
                new InitialHostDbBuilder(context).Create();

                //Default tenant seed (in host database).
                new DefaultTenantCreator(context).Create();
                new TenantRoleAndUserBuilder(context, 1).Create();
            }
            else
            {
                //You can add seed for tenant databases and use Tenant property...
            }

            context.SaveChanges();
        }
    }
}
