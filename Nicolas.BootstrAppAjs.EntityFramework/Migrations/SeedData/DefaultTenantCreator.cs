using System.Linq;
using Nicolas.BootstrAppAjs.EntityFramework;
using Nicolas.BootstrAppAjs.MultiTenancy;

namespace Nicolas.BootstrAppAjs.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly BootstrAppAjsDbContext _context;

        public DefaultTenantCreator(BootstrAppAjsDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
