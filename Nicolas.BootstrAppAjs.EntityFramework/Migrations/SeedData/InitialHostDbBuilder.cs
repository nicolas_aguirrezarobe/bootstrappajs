﻿using Nicolas.BootstrAppAjs.EntityFramework;
using EntityFramework.DynamicFilters;

namespace Nicolas.BootstrAppAjs.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly BootstrAppAjsDbContext _context;

        public InitialHostDbBuilder(BootstrAppAjsDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
