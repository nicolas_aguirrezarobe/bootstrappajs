﻿namespace Nicolas.BootstrAppAjs
{
    public class BootstrAppAjsConsts
    {
        public const string LocalizationSourceName = "BootstrAppAjs";

        public const bool MultiTenancyEnabled = true;
    }
}