﻿using Abp.Authorization;
using Nicolas.BootstrAppAjs.Authorization.Roles;
using Nicolas.BootstrAppAjs.Authorization.Users;

namespace Nicolas.BootstrAppAjs.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
