﻿using Abp.MultiTenancy;
using Nicolas.BootstrAppAjs.Authorization.Users;

namespace Nicolas.BootstrAppAjs.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}