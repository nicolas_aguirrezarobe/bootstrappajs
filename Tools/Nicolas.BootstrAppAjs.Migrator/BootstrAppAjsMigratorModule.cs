using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Nicolas.BootstrAppAjs.EntityFramework;

namespace Nicolas.BootstrAppAjs.Migrator
{
    [DependsOn(typeof(BootstrAppAjsDataModule))]
    public class BootstrAppAjsMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<BootstrAppAjsDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}