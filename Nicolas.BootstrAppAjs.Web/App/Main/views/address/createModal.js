﻿(function () {
    angular.module('app').controller('app.views.address.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.address',
        function ($scope, $uibModalInstance, addressService) {
            var vm = this;

            vm.address = {
                isActive: true
            };

            vm.save = function () {
				addressService.create(vm.address)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };
        }
    ]);
})();