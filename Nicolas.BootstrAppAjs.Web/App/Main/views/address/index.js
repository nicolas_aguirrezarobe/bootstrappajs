﻿(function () {
	angular.module('app').controller('app.views.address.index', [
		'$scope', '$uibModal', 'abp.services.app.address',
		function ($scope, $uibModal, addressService) {
			var vm = this;

			vm.address = [];

			function getAddress() {
				addressService.getAll({}).then(function (result) {
					vm.address = result.data.items;
				});
			}

			vm.openAddressCreationModal = function () {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/Main/views/address/createModal.cshtml',
					controller: 'app.views.address.createModal as vm',
					backdrop: 'static'
				});

				modalInstance.rendered.then(function () {
					$.AdminBSB.input.activate();
				});

				modalInstance.result.then(function () {
					getAddress();
				});
			};

			vm.refresh = function () {
				getAddress();
			};

			getAddress();
		}
	]);
})();