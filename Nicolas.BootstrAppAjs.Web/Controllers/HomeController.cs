﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace Nicolas.BootstrAppAjs.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : BootstrAppAjsControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}